'use strict'

const expect = require('chai').expect;
const R = require('ramda');

var { defineSupportCode } = require('cucumber');

defineSupportCode(function ({ Given, Then, When }) {
    Given(/^a ([\w|\s]+) payload$/, function (fixture) {
        this.payload = { body : JSON.stringify(require('../fixtures/valid-user')) };
    });

    When(/^I perform a (\w+) against the (.*) endpoint$/, function (verb, endpoint) {
        const that = this;
        return this.doOp(verb, endpoint, this.payload)
        .then(
            res => that.response = R.assoc("body", JSON.parse(res.body), res)
        );
    });

    Then('I receive a {int} status code', function (statuscode) {
        expect(this.response.statusCode).to.equal(statuscode);
    });

    Then('the payload for the new resource', function () {
        // todo: implement better contract checks here
        expect(this.response.body).to.exist;
    });

    Then('I can GET it by id', function () {
        return this.doOp('get', `/users/${this.response.body.data.id}`)
        .then(res => {
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.exist;
            return true;
        });
    })    
});
