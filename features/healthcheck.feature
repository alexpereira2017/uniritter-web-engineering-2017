Feature: health check

   As a sysadmin I want the API to provide a healthcheck endpoint
   so that I know when it is working or not.

   Scenario: Green status
   Given a running, healthy API
   When I perform a GET against the /healthcheck endpoint
   Then I receive a 200 status code
   And a green status message