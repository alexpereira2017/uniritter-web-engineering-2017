'use strict'

const cfg = require('./config');
const logger = require('./app/logger');

const Server = require('./app/server');
Server.createServer(cfg, logger).then(s => s.start());
