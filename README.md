# Web engineering sample

This repo contains the base app developed during the 2017 edition of the web engineering class, at UniRitter.

So in case you came here for serious functionality, sorry, kid, no dice. If you came for an example on things such as HATEOAS, OAuth, integrations over the web, 12-factor apps and other common concepts for cloud native apps, you may find one or two interesting things in the source code.

## What is this app about?

If implemented to its full potential, geekrep would be a stats aggregator across different tech services out there, such as bitbucket, github and stackoverflow. Thing of it as your one stop shop for nerdy bragging rights.


## Setup

You will either need to install node.js (suggested version: 6) and mongo (latest) on your machine, or use the provided docker-compose to bring the app up.


### Built on the shoulder of giants
[![auth0](https://bitbucket.org/repo/Eg6XXa9/images/2470565947-a0-badge-light.png)](https://auth0.com/?utm_source=oss&utm_medium=gp&utm_campaign=oss)