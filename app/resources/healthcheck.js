'use strict'

const healthcheck = module.exports = { }

healthcheck.registerRoutes = (server, logger) => {
    logger.debug('registering healthcheck endpoint');
    server.route({
        method: 'GET',
        path: '/healthcheck',
        config: {
            auth: false,
        },
        handler: function (request, reply) {
            logger.debug('received healthcheck ping');
            reply({ status: 'green' }).code(200);
        }
    });    
}