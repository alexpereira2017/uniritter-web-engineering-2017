'use strict';

var Joi = require('joi');

const Users = module.exports = { }

Users.registerRoutes = function (server, logger) {
    const 
        harvesterPlugin = server.plugins['hapi-harvester'],
        schema = {
            type: 'userStats',
            attributes: {
                source: Joi.string().length(100).required(),
                stats: Joi.array().items({
                    stat: Joi.string().required(),
                    count: Joi.number().required()
                })
            },
            relationships: {
                stats: { data: { type: "userStats" } }
            }
        }

    harvesterPlugin.routes.all(schema).forEach(function (route) {
        server.route(route);
    })
}